
public class Main {

    static final int DIMX = 10;
    static final int DIMY = 10;

    static Feld start;
    static Feld target;

    static Feld[][] spielwiese = new Feld[DIMX][DIMY];

    static class Quadrant1 implements Runnable {

        @Override
        public void run() {
            Feld.OAK oak;
            boolean tNotFound = true;
            boolean someMarked = true;
            Feld analyse = new Feld(start.x+1, start.y);
            int depth = 1;

            while (tNotFound) {

                if (isTarget(analyse)) {
                    spielwiese[analyse.x][analyse.y].depth = depth;
                    tNotFound = false;

                } else {
                    oak = analyse.oakland();
                    if (oak == Feld.OAK.IN) {
                        spielwiese[analyse.x][analyse.y].depth = depth;
                        someMarked = true;
                    }
                    analyse.x = analyse.x - 1;
                    analyse.y = analyse.y + 1;

                    if (analyse.x < (start.x+1) ) {
                        if (!someMarked) break;
                        depth++;
                        analyse.x = start.x + depth;
                        analyse.y = start.y;
                        someMarked = false;
                    }

                    if (analyse.y >= DIMY ) {
                        if (!someMarked) break;
                        depth++;
                        analyse.x = start.x + depth;
                        analyse.y = start.y;
                        someMarked = false;
                    }
                }
            }
        }
    }
    static class Quadrant2 implements Runnable {

        @Override
        public void run() {
            Feld.OAK oak;
            boolean tNotFound = true;
            boolean someMarked = true;
            Feld analyse = new Feld(start.x, start.y +1);
            int depth = 1;

            while (tNotFound) {

                if (isTarget(analyse)) {
                    spielwiese[analyse.x][analyse.y].depth = depth;
                    tNotFound = false;

                } else {
                    oak = analyse.oakland();
                    if (oak == Feld.OAK.IN) {
                        spielwiese[analyse.x][analyse.y].depth = depth;
                        someMarked = true;
                    }
                    analyse.x = analyse.x + 1;
                    analyse.y = analyse.y + 1;

                    if (analyse.x > start.x) {
                        if (!someMarked) break;
                        depth++;
                        analyse.x = start.x - depth +1;
                        analyse.y = start.y +1;
                        someMarked = false;
                    }

                    if (analyse.y >= DIMY) {
                        if (!someMarked) break;
                        depth++;
                        analyse.x = start.x - depth +1;
                        analyse.y = start.y +1;
                        someMarked = false;
                    }
                }
            }
        }
    }
    static class Quadrant3 implements Runnable {

        @Override
        public void run() {
            Feld.OAK oak;
            boolean tNotFound = true;
            boolean someMarked = true;
            Feld analyse = new Feld(start.x-1, start.y);
            int depth = 1;

            while (tNotFound) {

                if (isTarget(analyse)) {
                    spielwiese[analyse.x][analyse.y].depth = depth;
                    tNotFound = false;

                } else {
                    oak = analyse.oakland();
                    if (oak == Feld.OAK.IN) {
                        spielwiese[analyse.x][analyse.y].depth = depth;
                        someMarked = true;
                    }
                    analyse.x = analyse.x + 1;
                    analyse.y = analyse.y - 1;

                    if (analyse.x >= start.x) {
                        if (!someMarked) break;
                        depth++;
                        analyse.x = start.x - depth;
                        analyse.y = start.y;
                        someMarked = false;
                    }

                    if (analyse.y < 0) {
                        if (!someMarked) break;
                        depth++;
                        analyse.x = start.x - depth;
                        analyse.y = start.y;
                        someMarked = false;
                    }
                }
            }
        }
    }

    static class Quadrant4 implements Runnable {

        @Override
        public void run() {
            Feld.OAK oak;
            boolean tNotFound = true;
            boolean someMarked = true;
            Feld analyse = new Feld(start.x, start.y - 1);
            int depth = 1;

            while (tNotFound) {

                if (isTarget(analyse)) {
                    spielwiese[analyse.x][analyse.y].depth = depth;
                    tNotFound = false;

                } else {
                    oak = analyse.oakland();
                    if (oak == Feld.OAK.IN) {
                        spielwiese[analyse.x][analyse.y].depth = depth;
                        someMarked = true;
                    }
                    analyse.x = analyse.x + 1;
                    analyse.y = analyse.y + 1;

                    if (analyse.x >= DIMX) {
                        if (!someMarked) break;
                        depth++;
                        analyse.x = start.x;
                        analyse.y = start.y - depth;
                        someMarked = false;
                    }

                    if (analyse.y >= start.y) {
                        if (!someMarked) break;
                        depth++;
                        analyse.x = start.x;
                        analyse.y = start.y - depth;
                        someMarked = false;
                    }
                }
            }
        }
    }

    static void backtrack(int x, int y) {
        int smallest = spielwiese[x][y].depth;

        int t;
        while (!isStart(x, y)) {

            x = x - 1;
            if (x >= 0 && x < DIMX && y >= 0 && y < DIMY) {
                t = spielwiese[x][y].depth;
                if (t < smallest && t >= 0) {
                    smallest = t;
                    spielwiese[x][y].mark = '*';
                    continue;
                }
            }
            x = x + 2;
            if (x >= 0 && x < DIMX && y >= 0 && y < DIMY) {
                t = spielwiese[x][y].depth;
                if (t < smallest && t >= 0) {
                    smallest = t;
                    spielwiese[x][y].mark = '*';
                    continue;
                }
            }
            x = x - 1;
            y = y - 1;
            if (x >= 0 && x < DIMX && y >= 0 && y < DIMY) {
                t = spielwiese[x][y].depth;
                if (t < smallest && t >= 0) {
                    smallest = t;
                    spielwiese[x][y].mark = '*';
                    continue;
                }
            }
            y = y + 2;
            if (x >= 0 && x < DIMX && y >= 0 && y < DIMY) {
                t = spielwiese[x][y].depth;
                if (t < smallest && t >= 0) {
                    smallest = t;
                    spielwiese[x][y].mark = '*';
                    continue;
                }
            }
            // eine der 4 richtungen muss klappen, oder es ist das ziel.
            y = y - 1;
        }
    }

    static class Feld {
        public enum OAK {IN,WIDTH,HEIGHT,BOTH};

        public int x,y,depth;
        public char mark;

        public Feld(int xval, int yval) {
            depth = -1;
            x=xval;
            y=yval;
            mark = ' ';
        }

        public OAK oakland() {
            if (x < 0 || y < 0 || x >= DIMX || y >= DIMY) {
                if ( (x < 0 || x >= DIMX) && !(y < 0 || y >= DIMY)) return OAK.WIDTH;
                if (!(x < 0 || x >= DIMX) &&  (y < 0 || y >= DIMY)) return OAK.HEIGHT;

                return OAK.BOTH;
            } else {
                return OAK.IN;
            }
        }

    };

    public static boolean isTarget(int x, int y) {
        return (x == target.x &&  y == target.y);
    }

    public static boolean isStart(int x, int y) {
        return (x == start.x &&  y == start.y);
    }

    public static boolean isTarget(Feld f) {
        return isTarget(f.x, f.y);
    }

    public static boolean isStart(Feld f) {
        return isStart(f.x, f.y);
    }

    public static void printFeld() {
        for (int yi = 0; yi < DIMY; yi++) {
            for (int xi = 0; xi < DIMX; xi++) {
                if (isStart(xi, yi)) {
                    System.out.printf("[S]");
                } else if (isTarget(xi, yi)) {
                    System.out.printf("[T]");
                } else if (spielwiese[xi][yi].mark != ' ') {
                    System.out.printf(" %c ", spielwiese[xi][yi].mark);
                } else {
                    System.out.printf("%2d ", spielwiese[xi][yi].depth);
                }
            }
            System.out.printf("\n");
        }
    }

    public static void initSpielwiese() {
        for (int yi = 0; yi < DIMY; yi++) {
            for (int xi = 0; xi < DIMX; xi++) {
                if (isStart(xi,yi)) {
                    spielwiese[xi][yi] = start;
                } else if (isTarget(xi,yi)) {
                    spielwiese[xi][yi] = target;
                } else {
                    spielwiese[xi][yi] = new Feld(xi,yi);
                }
            }
        }
    }

    public static void main(String[] args) {
        start = new Feld(3,2);
        start.depth = 0;
        target = new Feld(7,7);

        initSpielwiese();

        Thread t1 = new Thread(new Quadrant1());
        Thread t2 = new Thread(new Quadrant2());
        Thread t3 = new Thread(new Quadrant3());
        Thread t4 = new Thread(new Quadrant4());

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        printFeld();
        System.out.print('\n');
        backtrack(target.x, target.y);
        printFeld();
    }
}
