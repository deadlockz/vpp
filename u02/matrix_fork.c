#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/wait.h>
#include <limits.h> // PIPE_BUF

// values below 10 print matricies

//#define CA 400
//#define RA 96
//#define CB 96
//#define RB 400

// not working with 1 process ?!?
//#define CA 8
//#define RA 16
//#define CB 16
//#define RB 8

// not working with 1 process ?!? Trouble with last values
//#define CA 8
//#define RA 12
//#define CB 12
//#define RB 8

#define CA 2000
#define RA 500
#define CB 500
#define RB 2000

long maxId = RA * CB;

// if you want to calculate it with your brain: try 5 instead of 100
#define RANDMAX 5
#define DATAMAX 510  // 510 best for PIPE_BUF = 4096 ??

long long* matrixA;
long long* matrixB;
long long* matrixC;

typedef struct s_Result {
    long id;
    long n;
    long long data[DATAMAX];
} Result;

void initializeMatrix(long long* matrix, int rows, int cols) {
    int i, j;

    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            *(matrix + j + cols * i) = rand() % RANDMAX;
        }
    }
}

void multiplyMatrices(Result * res) {
    int k;
    int i;
    int j;
    long runid;
    long id = res->id;
    
    for (runid = 0; runid < (res->n); ++runid) {
        i = id/CB;
        j = id - i*CB;
        
        for (k = 0; k < CA; k++) {
            // c[i][j] += a[i][k] * b[k][j]
            res->data[runid] += *(matrixA + k + CA * i) * *(matrixB + j + CB * k);
        }
        id++;
    }
    // padding ?!
    //if ((res->n) < DATAMAX) {
    //    for (runid = res->n; runid < DATAMAX; ++runid) {
    //        res->data[runid] = -1;
    //    }
    //}
}

void printMatrix(long long* matrix, int rows, int cols) {
    int i, j;

    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            printf("%7lld ", *(matrix + j + cols * i));
        }
        printf("\n");
    }
}


void childProcess(int nr, int proz, int * pipe_fd) {
    Result res;

    res.n = maxId / proz; //       <---- (1)
    res.id = (nr-1) * res.n;
    
    // der Letzte muss wegen dem Abrunden (1) ein paar mehr machen
    
    /*
     * beispiel: ergbenis 8x4 matrix = 32 zellen, berechnet von 3 Prozessen
     *  -> nr 1: toDoCells = 32/3 = 10
     *           startCell = 0         ( 0- 9)
     *  -> nr 2: toDoCells = 32/3 = 10
     *           startCell = 10        (10-19)
     *  -> nr 3: toDoCells = 32/3 = 10
     *           startCell = 20        (20-29)
     *         -> neee !!
     *           toDoCells = 32-20 =12  -> von Zelle 20 bis 31
     */
    
    if (nr == proz) res.n = maxId - res.id;
    
    if (res.n > DATAMAX || sizeof(res) > PIPE_BUF) {
        printf(
            "ERROR child process %d/%d wants to\n"
            " calc        %20ld but\n"
            " DATAMAX is  %20d\n"
            " Result size %20ld\n" 
            " PIPE_BUF is %20d\n", 
            nr, proz, res.n, DATAMAX, sizeof(res), PIPE_BUF
        );
        exit(1);
    }
    
    multiplyMatrices(&res);
    
    close(pipe_fd[0]);
    write(pipe_fd[1], (char *) &res, sizeof(Result));
    close(pipe_fd[1]);
}

int main(int argc, char * argv[]) {
    if (RA != CB || CA != RB) {
        printf("Invalid input, number of rows in MatrixA must "
        "be equal to number of columns in MatrixB and vice versa.\n");
        exit(-1);
    }
    if (argc < 2) {
        printf("%s [numbers of processes]\n", argv[0]);
        exit(-1);
    }
    int proz = atoi(argv[1]);
    
    int pid,i,j,p;
    long id;
    int pipe_fd[2]; /* file descriptros for the write and read ends of the pipe. */
    
    //printf("PIPE_BUF=%d\n", PIPE_BUF);
    pipe(pipe_fd);

    matrixA = (long long*) malloc(RA * CA * sizeof(long long));
    matrixB = (long long*) malloc(RB * CB * sizeof(long long));
    matrixC = (long long*) malloc(RA * CB * sizeof(long long));

    if (matrixA == NULL || matrixB == NULL || matrixC == NULL) {
        printf("Allocation error!");
        exit(-1);
    }

    //srand(time(NULL));
    srand(42); // makes results compareable

    initializeMatrix(matrixA, RA, CA);
    initializeMatrix(matrixB, RB, CB);

    if (RA <10 && CA<10) {
        printf("Matrix A:\n");
        printMatrix(matrixA, RA, CA);
        printf("Matrix B:\n");
        printMatrix(matrixB, RB, CB);
    }




    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    for (p = 1; p <= proz; ++p) {
        pid = fork();
        if (pid) {
            // parent
            //printf("Started child process %d with pid=%d\n", p, pid);
        } else {
            childProcess(p, proz, pipe_fd);
            exit(0);
        }
    }


    close(pipe_fd[1]);
    Result * res = (Result *) malloc(proz * sizeof(Result));
    
    for (p = 0; p < proz; ++p) {
        read(pipe_fd[0], (char *) &(res[p]), sizeof(Result));
    }
    close(pipe_fd[0]);
    
    for (p = 0; p < proz; ++p) {
        wait(NULL);  // wait for some child to exit.
    }

    for (p = 0; p < proz; ++p) {
        for (id=0; id < res[p].n; ++id) {
            i = (id+res[p].id) / CB;
            j = (id+res[p].id) - i*CB;
            *(matrixC + j + CB * i) = res[p].data[id];
        }
    }
    
    
    clock_gettime(CLOCK_MONOTONIC, &end);
    //if (RA <10 && CA<10) {
        printf("Ergebnis:\n");
        printMatrix(matrixC, RA, CB);
    //}
    
    double runtime = (end.tv_sec - start.tv_sec);
    runtime += (end.tv_nsec - start.tv_nsec) / 1000000000.0;
    printf("%.2f s\n", runtime);

    // if you use malloc (=new) you have to make free (= delete) ?!
    free(res);
    free(matrixA);
    free(matrixB);
    free(matrixC);
    
    return 0;
}

