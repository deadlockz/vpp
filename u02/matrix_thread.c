/**
 * Example
 * 
 * > gcc -Wall -pthread matrix_thread.c 
 * > ./a.out 3
 * ColumnsB = 500 is not splitable into 3 threads!
 * > ./a.out 10
 * 10.68 s
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>

// values below 10 print matricies

//#define CA 400
//#define RA 96
//#define CB 96
//#define RB 400

#define CA 2000
#define RA 500
#define CB 500
#define RB 2000

// if you want to calculate it with your brain: try 5 instead of 100
#define RANDMAX 5

typedef struct s_Params {
    long long* matrixA;
    long long* matrixB;
    long long* matrixC;
    int i;
    int j;
} Params;

void initializeMatrix(long long* matrix, int rows, int cols) {
	int i, j;

	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			*(matrix + j + cols * i) = rand() % RANDMAX;
		}
	}
}

void * multiplyMatrices(void * what) {
    int k;
    Params * p = (Params *) what;
    for (k = 0; k < CA; k++) {
        // c[i][j] += a[i][k] * b[k][j]
        *(p->matrixC + p->j + CB * p->i) += *(p->matrixA + k + CA * p->i) * *(p->matrixB + p->j + CB * k);
    }
    return NULL;
}

void printMatrix(long long* matrix, int rows, int cols) {
	int i, j;

	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			printf("%7lld ", *(matrix + j + cols * i));
		}
		printf("\n");
	}
}

int main(int argc, char * argv[]) {
	if (RA != CB || CA != RB) {
		printf("Invalid input, number of rows in MatrixA must "
        "be equal to number of columns in MatrixB and vice versa.\n");
		exit(-1);
	}
    if (argc < 2) {
        printf("%s [numbers of threads]\n", argv[0]);
        exit(-1);
    }
    int threads = atoi(argv[1]);
    
    if ( CB%threads > 0) {
        printf("ColumnsB = %d is not splitable into %d threads!\n", CB, threads);
        exit(-1);
    }
	
	long long* matrixA = (long long*) malloc(RA * CA * sizeof(long long));
	long long* matrixB = (long long*) malloc(RB * CB * sizeof(long long));
	long long* matrixC = (long long*) malloc(RA * CB * sizeof(long long));

    pthread_t * threadPtr = (pthread_t*) malloc(threads * sizeof(pthread_t));
    Params*     params    = (Params*) malloc(threads * sizeof(Params));

	if (matrixA == NULL || matrixB == NULL || matrixC == NULL || threadPtr == NULL || params == NULL) {
		printf("Allocation error!");
		exit(-1);
	}
	
	//srand(time(NULL));
	srand(42); // makes results compareable
    
	initializeMatrix(matrixA, RA, CA);
	initializeMatrix(matrixB, RB, CB);
    
    if (RA <10 && CA<10) {
        printf("Matrix A:\n");
        printMatrix(matrixA, RA, CA);
        printf("Matrix B:\n");
        printMatrix(matrixB, RB, CB);
    }


	struct timespec start, end;
	clock_gettime(CLOCK_MONOTONIC, &start);
	
    int i,j,t;
    for (i = 0; i < RA; i++) {
        for(j = 0; j < CB; ) {
            for(t = 0; t < threads; t++) {
                params[t].i = i;
                params[t].j = j;
                params[t].matrixA = matrixA;
                params[t].matrixB = matrixB;
                params[t].matrixC = matrixC;
                //multiplyMatrices(&(params[t]));
                //printf("create t%d\n", t);
                pthread_create(&(threadPtr[t]), NULL, multiplyMatrices, &(params[t]));
                j++;
            }
            for (t=0; t < threads; ++t) {
                //printf("wait for t%d\n", t);
                pthread_join(threadPtr[t], NULL);
            }
        }
    }
	
	clock_gettime(CLOCK_MONOTONIC, &end);
    
    //if (RA <10 && CA<10) {
        printf("Ergebnis:\n");
        printMatrix(matrixC, RA, CB);
    //}

	double runtime = (end.tv_sec - start.tv_sec);
	runtime += (end.tv_nsec - start.tv_nsec) / 1000000000.0;
	printf("%.2f s\n", runtime);

    // if you use malloc (=new) you have to make free (= delete) ?!
	free(matrixA);
	free(matrixB);
	free(matrixC);
    free(threadPtr);
    free(params);
    
    return 0;
}

