package de.digisocken;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 Lock
 sharedValue: 3000000
 Time: 6317797866 ns

 tryLock
 sharedValue: 3000000
 Time: 750944160 ns
 */
public class Main {
    static SpinLock _spidi =new SpinLock();
    public static final int m = 20; // threads
    public static final int n = 150000; // increments each thread
    public static int sharedValue = 0;

    static private class MyThreadA implements Runnable {

        @Override
        public void run() {
            for(int i=0; i<n; ) {
                _spidi.lock();
                sharedValue++;
                _spidi.unlock();
                i++;
            }
        }
    };

    static private class MyThreadB implements Runnable {

        @Override
        public void run() {
            for(int i=0; i<n; ) {
                try {
                    if (_spidi.tryLock(500, TimeUnit.NANOSECONDS)) {
                        sharedValue++;
                        _spidi.unlock();
                        i++;
                    }
                } catch (InterruptedException e) {}
            }
        }
    };

    /*
    static private class MyThreadX implements Runnable {
        int _nr;

        MyThreadX(int nr) {
            _nr = nr;
        }

        @Override
        public void run() {
            _spidi.lock();
            for(int i=0; i<n; ++i) {
                System.out.printf(
                        "I am Thread %d and say %d\n",
                        _nr, i
                );
            }
            _spidi.unlock();
        }
    };
    */

    public static void main(String[] args) throws InterruptedException {
        long tstart, tend;
        ArrayList<Thread> threadsA = new ArrayList<>(m);
        ArrayList<Thread> threadsB = new ArrayList<>(m);

        /*
        ArrayList<Thread> threadsX = new ArrayList<>(m);
        for(int i=0; i<m; ++i) {
            threadsX.add(i, new Thread(new MyThreadX(i+1)));
            threadsX.get(i).start();
        }

        for(Thread thr : threadsX) {
            thr.join();
        }
        */


        for(int i=0; i<m; ++i) {
            threadsA.add(i, new Thread(new MyThreadA()));
        }
        for(int i=0; i<m; ++i) {
            threadsB.add(i, new Thread(new MyThreadB()));
        }

        sharedValue = 0;
        System.out.println("Lock");

        tstart = System.nanoTime();
        for(Thread thr : threadsA) {
            thr.start();
        }
        for(Thread thr : threadsA) {
            thr.join();
        }
        tend = System.nanoTime();

        System.out.printf("sharedValue: %d\n", sharedValue);
        System.out.printf("Time: %d ns\n\n", tend-tstart);

        // --------------------------------------
        sharedValue = 0;
        System.out.println("tryLock");

        tstart = System.nanoTime();
        for(Thread thr : threadsB) {
            thr.start();
        }
        for(Thread thr : threadsB) {
            thr.join();
        }
        tend = System.nanoTime();

        System.out.printf("sharedValue: %d\n", sharedValue);
        System.out.printf("Time: %d ns\n", tend-tstart);

    }
}
