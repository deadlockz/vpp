package de.digisocken;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


public class SpinLock implements Lock {

    private AtomicBoolean isLocked;

    public SpinLock() {
        isLocked = new AtomicBoolean(false);
    }

    // Versucht die Sperre so lange zu erwerben, bis es diese bekommt.
    public void lock() {
        while (tryLock() == false);
    }

    // Gibt die Sperre frei.
    public synchronized void unlock() {
        isLocked.set(false);
        notify(); // methoden mit notify() und wait() müssen je synchronised sein
    }

    // Erwirbt die Sperre nur dann, wenn es zu diesem Zeitpunkt frei ist.
    @Override
    public boolean tryLock() {
        return isLocked.compareAndSet(false,true);
    }

    // Erwirbt die Sperre, wenn sie innerhalb der vorgegebenen Wartezeit frei ist.
    //TimeUnit in Nanosec oder zumindest die gleiche Einheit wie time
    @Override
    public synchronized boolean tryLock(long time, TimeUnit unit) throws InterruptedException {

        if (tryLock() == false) {
            wait(time); // methoden mit notify() und wait() müssen je synchronised sein
            return tryLock();
        } else {
            return true;
        }
    }

    // ---------------------------------------------------------------

    @Override
    public Condition newCondition() {
        // Muss nicht implementiert werden.
        return null;
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        // Muss nicht implementiert werden.
    }

    //1.e) Testen Sie den erstellten Spinlock. Erstellen Sie hierzu eine Anwendung,
    // 		welche m Threads eine Variable jeweils n mal inkrementiert und
    // 		stellen Sie sicher, dass das Ergebnis korrekt ist.

    //2.) 	Vergleichen verschiedener Lock-Implementierungen
    //		Schreiben Sie einen kleinen Benchmark, der Ihre Spinlock-Implementierung
    //		mit folgenden anderen Synchronisierungsmoeglichkeiten hinsichtlich
    //		Laufzeit vergleicht:
    //		 synchronized
    //		 reeantrant locks (fair und unfair)
    //		 stamped locks
}
