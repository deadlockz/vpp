
# Spinlock mit AtomicBoolean

![](bild1.png)

# Verwendung in Threads

![](bild2.png)

# Details

trylock() versucht die Atomic Variable zu setzen (auf true), falls diese false ist.

lock() läuft solange "im Spin/Kreis", bis tryLock() erfolgreich gesetz werden kann. Das ist der Grund, warum ein Thread stoppt, weil er in einer while-schleife hängt.

unlock() ist trivial - wichtig ist nur das notify(), um einem wait() an einer anderen Stelle bescheid zu geben, dass die "isLocked" Variable nun frei wurde.

trylock(ns) ist die etwas andere Version, die einmal kurz testet, ob sie "isLocked" setzen kann. trylock wartet dann mit wait(ns) auf ein notify(), um es dann nochmal zu versuchen. Klappt es in der Zeit und kurz danach nicht mit dem "Locked", gibt diese Methode ein "false" zurück. -> Der Thread, der dieses trylock() statt lock() nutzt, muss mit dem "false" zurechtkommen -> in meinem Beispiel wird die Exception unterdrückt und nicht Inkrementiert.

sind wait() und notify() im Spiel, müssen die Methoden "synchronized" sein.
